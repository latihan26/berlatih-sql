Tugas Hari 2

Nomor 1
CREATE DATABASE myshop;

Nomor 2
TABLE USERS
CREATE TABLE users( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );
TABLE CATEGORIES
CREATE TABLE categories( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null );
TABLE ITEMS
CREATE TABLE items( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, description varchar(255) NOT null, price int(8) NOT null, stock int(8) NOT null, category_id int(8) NOT null, FOREIGN KEY(category_id) REFERENCES categories(id) );

Nomor 3
insert data users
INSERT INTO users(name,email,PASSWORD) VALUES("john doe","john@doe.com","john123"), ("Jane Doe","jane@doe.com","jenita123");
insert data categories
INSERT INTO categories(name) VALUES ("gadget"),("cloth"),("men"),("women"),("branded");
insert data items
INSERT INTO items(name, description, price, stock, category_id) VALUES("Sumsang b50","hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh","baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch","jam tangan anak yang jujur banget", 2000000, 10, 1);

Tugas Hari 3

Soal 1
a.Mengambil data users kecual password

SELECT id,name,email from users;

b.Mengambil data items

*Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).

SELECT * FROM items WHERE price > 1000000;

*Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).

SELECT * FROM items WHERE name LIKE "%uniklo%";

C. Menampilkan data items join dengan kategori

SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori FROM items INNER JOIN categories on items.category_id = categories.id;

soal 2

UPDATE items SET price=2500000 WHERE id = 1;


